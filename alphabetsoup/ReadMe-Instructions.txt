Name: Alphabet Soup Game
Author: Stephen A. Michael
Email: smichaelcos@comcast.net

Date: January 14, 2021

Description:
    This application is my interpretation of the "Alphabet Soup" programming challenge
    from from https://gitlab.com/enlighten-challenge/alphabet-soup.

Source File Locations:
    alphabet-soup/alphabetsoup/app/src/main/java/com/smichael/alphabetsoup/
        App.java
        Coords.java
        Match.java
        WordGame.java

Test class
    alphabet-soup/alphabetsoup/app/src/test/java/com/smichael/alphabetsoup/
        AppTest.java

Test / Input File Location:
    alphabet-soup/alphabetsoup/app/src/main/resources/ASoup00.txt
                                                      ASoup01.txt
                                                      ASoup02.txt

Invocation (from alphabet-soup/alphabetsoup path):
./gradlew run --args=ASoup00.txt

Also note that there is an optional 2nd argument 'v', which causes the puzzle to be printed
./gradlew run --args='ASoup00.txt v'

To invoke the test cases using Gradle and junit:
./gradlew test
