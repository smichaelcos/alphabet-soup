/**
 * class Match defines the attributes (start and end coordinates) of a word match
 * within the puzzle.
 */
package com.smichael.alphabetsoup;

class Match {
    private Coords  startMatchAddr, endMatchAddr;

    public Match(int sR, int sC, int eR, int eC) {
        startMatchAddr = new Coords(sR, sC);
        endMatchAddr   = new Coords(eR, eC);
    }

    /**
     * toString()
     *
     * Generate the String representation of a puzzle matching word location.
     * For example: '0:0 4:4' would reflect a 5 character word that matches
     * on a diagonal running from the top left corner of the puzzle down towards
     * the right corner.
     */
    public String toString() {
        String outStr;
        outStr = startMatchAddr.toString() + " " + endMatchAddr.toString();
        return outStr;
    }
} // Match
