/*
 * Name: Alphabet Soup Game
 * Invocation: AlphabetSoup <gamefilename.txt>
 * Author: Stephen A. Michael
 * Date: January 14, 2021
 * Description:
 *     This application is my interpretation of the "Alphabet Soup" programming challenge
 *     from from https://gitlab.com/enlighten-challenge/alphabet-soup, and is described in
 *     the following text from that site.
 *
 * To run the application from Gradle, the input file must be in the resources directory
 * and can be run as follows:
 * ./gradlew run --args=ASoup00.txt
 *
 * Also note that there is an optional 2nd argument 'v', which causes the puzzle to be printed
 * ./gradlew run --args='ASoup00.txt v'
 *
 * before the solution.
 * --------------------------------------------------------------------------------------------------------------
 * You have been contracted from a newspaper tasked with the job of providing an answer key to their word
 * search for the Sunday print.  The newspaper's word search is a traditional game consisting of a grid of
 * characters in which a selection of words have been hidden.  You are provided with the list of words that
 * have been hidden and must find the words within the grid of characters.
 *
 * Requirements:
 * -------------
 *   Load a character grid with scrambled words embedded within it and a words list of the words to find.
 *   The following conditions apply:
 *   - Within the grid of characters, the words may appear vertical, horizontal or diagonal.
 *   - Within the grid of characters, the words may appear forwards or backwards.
 *   - Words that have spaces in them will not include spaces when hidden in the grid of characters.
 *
 * Input Format:
 * -------------
 *   - The program is to accept a file as input. The file is an ASCII text file containing the word search board
 *     along with the words that need to be found.
 *   - The file contains three parts. The first part is the first line, and specifies the number of rows and columns
 *     in the grid of characters, separated by an 'x'. The second part provides the grid of characters in the word
 *     search. The third part in the file specifies the words to be found.
 *   - The first line indicates how many following lines in the file contain the rows of characters that make up the
 *     word search grid.  Each row in the word search grid will have the specified number of columns of characters,
 *     each separated with a space.  The remaining lines in the file specify the words to be found.
 *   - The file format is as follows.  Note that "|" characters are not included in the file but shown here for clarity:
 *         |3x3|
 *         |A B C|
 *         |D E F|
 *         |G H I|
 *         |ABC|
 *         |AEI|
 *   - You may assume that the input files are correctly formatted. Error handling for invalid input files may be ommitted.
 *
 * Output Format:
 * --------------
 *   - The output will specify the word found, along with the indices specifying where the beginning and ending characters
 *     of the word are located in the grid. A single space character will separate the word from the beginning and ending
 *     indices. The order of the words in the output should remain the same as the order of the words specified in the
 *     input file. The program will output to screen or console (and not to a file).
 *   - The word search grid row and column numbers can be used to identify the location of individual characters in the board.
 *     For example, row 0 column 0 (represented as 0:0) is the top-left character of a 3x3 board.
 *     The bottom-right corner of a 3x3 board would be represented as 2:2.
 *         |ABC 0:0 0:2|
 *         |AEI 0:0 2:2|
 *
 * Sample Data:
 * ------------
 *   - The following may be used as sample input and output datasets.
 *   - Input follows.  Note that "|" characters are not included in the file but shown here to clarify input.
 *         |5x5|
 *         |H A S D F|
 *         |G E Y B H|
 *         |J K L Z X|
 *         |C V B L N|
 *         |G O O D O|
 *         |HELLO|
 *         |GOOD|
 *         |BYE|
 *
 *   - Ouput follows.  Note that "|" characters are not included in the output but shown here for clarity.
 *         |HELLO 0:0 4:4|
 *         |GOOD 4:0 4:3|
 *         |BYE 1:3 1:1|
 */
package com.smichael.alphabetsoup;

public class App {
    public String getGreeting() {
        return "Alphabet Soup Game";
    }

    /**
     * main()
     *
     * The main entry point which:
     * 1. Loads the puzzle
     * 2. Loads the words to be found
     * 3. Outputs the solution
     */
    public static void main(String[] args) {
        WordGame theGame;
        boolean  bVerbose = false;
        if (args.length < 1) {
            // No input file was specified.  Pass null to trigger read from stdin.
            theGame = new WordGame(null);
        }
        else {
            // An input filename was specified.  Pass it to the game.
            theGame = new WordGame(args[0]);
        }

        if (args.length >= 2)
        {
            // Verbose argument - this will cause the puzzle to be echoed.
            if (args[1].toUpperCase().substring(0,1).equals("V"))
            {
                bVerbose = true;
            }
        }

        //System.out.println("Enter the Puzzle");
        theGame.loadPuzzle();
        if (bVerbose)
        {
            theGame.echoPuzzle();
            System.out.println();
        }

        theGame.loadWords();
        //theGame.echoWords();

        for (int w=0; w<theGame.wordCount(); w++) {
            Match match = theGame.findWord(theGame.getWord(w));
            if (match == null) {
                System.out.println("ERROR: Word: " + theGame.getWord(w) + " was not found!");
            }
            else {
                System.out.println(theGame.getWord(w) + " " + match.toString());
            }
        }
    }
}
