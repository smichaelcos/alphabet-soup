/**
 * class WordGame describes a "puzzle" word game and allows interaction with it.
 */
package com.smichael.alphabetsoup;

import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;

class WordGame {
    private Matrix      puzzle;     // A matrix of the letters within the puzzle
    private Scanner     scanner;
    private Dictionary  dictionary; // A dictionary of words contained within the puzzle
    // private ClassLoader myClassLoader;

    public WordGame(String puzzleTextFName) {

        if (puzzleTextFName == null) {
            /*
             * No input file specified.  Read from stdin
             */
            scanner = new Scanner(System.in);
        }
        else {
            /*
             * An input file was specified
             */
            // File inFile = new File(puzzleTextFName);
            ClassLoader classLoader = getClass().getClassLoader();

            File inFile = new File(classLoader.getResource(puzzleTextFName).getFile());
            try {
                scanner = new Scanner(inFile);
            }
            catch (FileNotFoundException ex) {
                System.out.println("ERROR: Input file: " + puzzleTextFName + " not found!");
                System.exit(1);
            }
        }

        dictionary = new Dictionary();
        return;
    }

    /**
     * wordCount() - Return a count of the number of words within the puzzle.
     */
    public int wordCount() {
        return dictionary.size();
    }

    /**
     * getWord() - Return the word at the specific 0-based index.
     */
    public String getWord(int idx) {
        return dictionary.get(idx);
    }

    /**
     * echoWords() - Print the words found within the puzzle.
     */
    public void echoWords() {
        for (int w=0; w<dictionary.size(); w++) {
            System.out.println(dictionary.get(w));
        }
    }

    /**
     * class Dictionary defines the words that are in the puzzle
     */
    class Dictionary extends ArrayList<String> {

        public Dictionary() {
            super();
        }

        /**
         * addWord() - Add a new word to the puzzle
         */
        public int addWord(String sWord) {
            this.add(sWord);
            return this.size() - 1;
        }

        /**
         * loadWords() - Load all the words from the input file.
         */
        public int loadWords() {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine().trim().toUpperCase();
                addWord(line);
            }
            return this.size();
        }
    } // Dictionary

    /**
     * loadPuzzle()
     *
     * Load a puzzle from the input file, according to the predefined format.
     */
    public void loadPuzzle() {
        String  params[] = scanner.nextLine().trim().split("x");

        int rows = Integer.parseInt(params[0]);
        int cols = Integer.parseInt(params[1]);

        puzzle = new Matrix(rows, cols);
        for (int r=0; r<rows; r++) {
            String rowString = scanner.nextLine().trim().toUpperCase();
            puzzle.setRow(r, rowString);
        }
        return;
    }

    /**
     * loadWords()
     *
     * Load the words to be found in the puzzle, from the input file.
     */
    public int loadWords() {
        return dictionary.loadWords();
    }

    /**
     * echoPuzzle()
     *
     * Echo the puzzle that has been loaded
     */
    public void echoPuzzle() {
        for (int r=0; r<puzzle.getRowCount(); r++) {
            for (int c=0; c<puzzle.getColCount(); c++) {
                System.out.print(puzzle.getLetterAt(r,c) + " ");
            }
            System.out.println();
        }
    }

    /**
     * class Matrix defines the characteristics of a word puzzle, in an N x M format
     */
    class Matrix {
        int    rows, cols;
        public int getRowCount() { return rows; }
        public int getColCount() { return cols; }

        char[][] matrix;

        public Matrix(int rows, int cols) {
            this.rows = rows;
            this.cols = cols;
            matrix = new char[rows][cols]; // The character array of the letters within the puzzle
            return;
        }

        /**
         * getLetterAt()
         *
         * Returns the letter at a specific 0-based row, column index within the puzzle.
         */
        public char getLetterAt(int rowIdx, int colIdx) {
            return this.matrix[rowIdx][colIdx];
        }

        /**
         * setRow()
         *
         * Set the contents of a puzzle row, from a formatted input as defined from the standard.
         * For example, an input string for a puzzle that is five (5) letters wide (separated by
         * spaces) such as in this example: "H A S D F"
         */
        public void setRow(int rowIdx, String inString) {
            // Check valid rowIdx and valid string length
            String[] row = inString.trim().split(" ");
            for (int c=0; c<cols; c++)
            {
                matrix[rowIdx][c] = row[c].charAt(0);
            }
        }
    } // Matrix

    /**
     * getRowCount(), getColCount() returns the puzzle matrix row,col counts.
     */
    public int getRowCount() { return puzzle.getRowCount(); }
    public int getColCount() { return puzzle.getColCount(); }

    /**
     * reverse() - Return the reverse of the letters within the input string.
     */
    private String reverse(String inString) {
        StringBuilder sb = new StringBuilder(inString);
        sb.reverse();
        return sb.toString();
    }

    /**
     * getRow() - get the designated row as a string, with no separating spaces.
     */
    public String getRow(int rowIdx) {
        String rowValue="";
        for (int c=0; c<puzzle.getColCount(); c++) {
            rowValue += String.valueOf(puzzle.getLetterAt(rowIdx, c));
        }

        return rowValue;
    }

    /**
     * getCol() - get the designated column as a string, with no separating spaces.
     */
    public String getCol(int colIdx) {
        String colValue="";
        for (int r=0; r<puzzle.getRowCount(); r++) {
            colValue += String.valueOf(puzzle.getLetterAt(r, colIdx));
        }

        return colValue;
    }

    /**
     * getDiagLR() - get the designated puzzle diagnonal, oriented from the top left
     * to the bottom right of the puzzle, with no separating spaces.
     */
    public String getDiagLR(int rowIdx, int colIdx) {
        String diagValue="";
        int c = colIdx;
        for (int r=rowIdx; r<puzzle.getRowCount() && c<puzzle.getColCount(); r++, c++) {
            diagValue += String.valueOf(puzzle.getLetterAt(r,c));
        }

        return diagValue;
    }

    /**
     * getDiagRL() - get the designated puzzle diagnonal, oriented from the top right
     * to the bottom left of the puzzle, with no separating spaces.
     */
    public String getDiagRL(int rowIdx, int colIdx) {
        String diagValue="";
        int c = colIdx;
        for (int r=rowIdx; r<puzzle.getRowCount() && c>=0; r++, c--) {
            diagValue += String.valueOf(puzzle.getLetterAt(r,c));
        }

        return diagValue;
    }

    /**
     * findWord() - Find the specified word within the puzzle, returning its match address.
     * If the word is not found, a null will be returned.
     */
    public Match findWord(String word) {
        int idx;

        String searchWord  = word.replace(" ", ""); // Remove any spaces
        String searchRWord = reverse(searchWord);

        /*
         *  Look for the word along the rows within the puzzle
         */
        for (int r=0; r<getRowCount(); r++) {
            idx = getRow(r).indexOf(searchWord);
            if (idx != -1) {
                // Found the word within the row in normal left to right word placement.  Return the match address.
                return new Match(r,idx, r,idx+searchWord.length()-1);
            }

            idx = getRow(r).indexOf(searchRWord); // Reverse search for word within the row

            if (idx == -1) {
                // No match.  Coninue the search.
                continue;
            }

            // Matched a reverse placement of the word within in the row!  Return the match address to the caller.
            return new Match(r,idx+searchWord.length()-1, r,idx);
        }

        /*
         * Look for the word along the columns within the puzzle.
         */
        for (int c=0; c<getColCount(); c++) {
            idx = getCol(c).indexOf(searchWord);
            if (idx != -1) {
                // Found the word, from top-to-bottom, in the column.  Return the match address.
                return new Match(idx,c, idx+searchWord.length()-1,c);
            }

            idx = getCol(c).indexOf(searchRWord); // Reverse search for word.

            if (idx == -1) {
                continue;
            }

            // Found the word, from bottom-to-top, in the column!  Return the match address.
            return new Match(idx+searchWord.length()-1,c, idx,c);
        }

        /*
         * Look for the word along the left-to-right diagnonals within the puzzle.
         */
        for (int r=0; r<getRowCount(); r++) {
            for (int c=0; c<getColCount(); c++) {
                idx = getDiagLR(r,c).indexOf(searchWord);
                if (idx != -1) {
                    // Found the word as placed from top to bottom.  Return the match address.
                    return new Match(r+idx,c+idx, r+searchWord.length()-1,c+searchWord.length()-1);
                }

                idx = getDiagLR(r,c).indexOf(searchRWord); // Reverse search for word.
                if (idx == -1) {
                    continue;
                }

                // Found a reverse match on the diagonal
                return new Match(r+searchWord.length()-1,c+searchWord.length()-1, r+idx,c+idx);
            }
        }

        /*
         * Look for the word along the right-to-left diagnonals within the puzzle.
         */
        for (int r=0; r<getRowCount(); r++) {
            for (int c=getColCount()-1; c>=0; c--) {
                idx = getDiagRL(r,c).indexOf(searchWord);
                if (idx != -1) {
                    // Found the word as placed from top to bottom.  Return match address.
                    return new Match(r+idx,c-idx, r+searchWord.length()-1,c-searchWord.length()+1);
                }

                idx = getDiagRL(r,c).indexOf(searchRWord); // Reverse search for word.
                if (idx == -1) {
                    continue;
                }

                // Found a reverse match on RL the diagonal.
                return new Match(r+searchWord.length()-1,c-searchWord.length()+1, r+idx,c-idx);
            }
        }

        /*
         * No match found for the word!
         */
        return null;
    }
} // WordGame
