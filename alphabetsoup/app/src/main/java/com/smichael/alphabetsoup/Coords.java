/**
 * class Coords defines the zero-based row, column indices for letters within the puzzle.
 * For example: row 0, col 0 is the top left row and column within the puzzle.
 */
package com.smichael.alphabetsoup;

class Coords {
    private int row, col;

    public Coords(int row, int col) {
        this.row = row;
        this.col = col;
    }

    /**
     * toString()
     *
     * Generate the String representation of a puzzle's letter coordinate.
     * For example: '0:0' is the top left coordinate.
     */
    public String toString() {
        String coord = row + ":" + col;
        return coord;
    }
} // Coords
